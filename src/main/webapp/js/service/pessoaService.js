angular
	.module('crud')
	.service('pessoaService', ['$http', '$q', pessoaService]);

function pessoaService($http, $q) {

	this.findAll = findAll;
	this.save = save;
	this.remove = remove;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./pessoa/all')
			.then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});
		return deferred.promise;
	}

	function save(pessoa) {
		var deferred = $q.defer();
		if(pessoa.id) {
			$http
				.put('./pessoa/' + pessoa.id, pessoa)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error.data);
				});
		} else {
			$http
				.post('./pessoa/add', pessoa)
				.then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error.data);
				});
		}
		return deferred.promise;
	}

	function remove(id) {
		var deferred = $q.defer();
		$http
			.delete('./pessoa/' + id)
			.then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});
		return deferred.promise;
	}

}
