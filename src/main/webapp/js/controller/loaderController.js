angular
	.module('crud')
	.controller('LoaderController', ['$rootScope', LoaderController]);

function LoaderController($rootScope) {

	$rootScope.loader = {
		visible: false
	};

}
