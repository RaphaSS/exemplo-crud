angular
	.module('crud')
	.controller('PessoaController', ['$scope', '$uibModal', 'pessoaService', '$interval', PessoaController]);

function PessoaController($scope, $uibModal, pessoaService, $interval) {

	$scope.maxDate = new Date();

	$scope.filtro = {
		pessoa: {},
		datas: {}
	};

	$scope.mostrarOuEsconderFiltros = function() {
		if(!$scope.filtro.exibir) {
			delete $scope.filtro.pessoa.cpf;
			delete $scope.filtro.datas;
		} else {
			$scope.filtro.datas = {
				to: $scope.maxDate
			};
		}
	};

	$scope.addPessoa = function(pessoa) {
		$uibModal
			.open({
				templateUrl: './views/partials/modals/add-pessoa.tpl.html',
				controller: 'AddPessoaController',
				size: 'lg',
				resolve: {
					pessoa: function() {
						return pessoa;
					}
				}
			}).result.then(load);
	};

	$scope.removerPessoa = function(pessoa) {
		$scope.confirm('Excluir ' + pessoa.nome + '?', 'Você deseja realmente excluir ' + pessoa.nome + '?', function() {
			pessoaService
				.remove(pessoa.id)
				.then(load);
		});
	};

	function load() {
		$scope.loading(true);
		pessoaService
			.findAll()
			.then(function(pessoas) {
				$scope.pessoas = pessoas;
				$scope.loading(false);
			});
	}

	load();

}
