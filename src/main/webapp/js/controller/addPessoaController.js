angular
	.module('crud')
	.controller('AddPessoaController', ['$scope', '$uibModalInstance', 'pessoa', 'pessoaService', AddPessoaController]);

function AddPessoaController($scope, $uibModalInstance, pessoa, pessoaService) {

	$scope.data = {
		isNew: !pessoa || !pessoa.id
	};
	$scope.pessoa = angular.copy(pessoa);

	$scope.save = function() {
		if($scope.form.$valid) {
			pessoaService
				.save($scope.pessoa)
				.then(function() {
					$uibModalInstance.close();
				}, function(error) {
					$scope.alert(error.cause.sqlexception ?
						error.cause.sqlexception.message : error.cause.message);
				});
		} else {
			angular.element('form .ng-invalid:first').focus();
		}
	}

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	}

}
