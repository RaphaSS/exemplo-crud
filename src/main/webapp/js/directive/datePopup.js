angular
	.module('crud')
	.directive('datePopup', DatePopupDirective);

function DatePopupDirective($interval) {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			ngRequired: '=',
			ngModel: '=',
			minDate: '=',
			maxDate: '='
		},
		templateUrl: './views/partials/directives/datepopup.tpl.html',
		link: function(scope, elm, attrs, ctrl) {
			scope.datepickerOptions = {
				minDate: scope.minDate,
				maxDate: scope.maxDate
			};

			scope.$watch('minDate', function() {
				scope.datepickerOptions.minDate = scope.minDate;
			});
			scope.$watch('maxDate', function() {
				scope.datepickerOptions.maxDate = scope.maxDate;
			});

			scope.status = {
				opened: false
			};
		}
	}
}
