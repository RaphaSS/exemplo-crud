function getComplexValue(owner, attr) {
	if(attr && owner) {
		var dot = attr.indexOf('.');
		if(dot === -1) {
			return owner[attr];
		}
		var first = attr.substring(0, dot);
		return getComplexValue(owner[first], attr.substring(dot + 1));
	} else {
		return owner;
	}
}
