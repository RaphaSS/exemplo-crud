angular
	.module('crud', ['ngMask', 'ui.bootstrap'])
	.run(['$rootScope', 'bootbox', function($rootScope, bootbox) {

		$rootScope.loading = function(visible) {
			$rootScope.loader.visible = !!visible;
		};

		$rootScope.confirm = function(title, message, fnYes, fnNo) {
			bootbox.dialog({
				title: title,
				message: message,
				closeButton: false,
				buttons: {
					'Sim': {
						className: 'btn-primary',
						callback: fnYes
					},
					'Não': {
						className: 'btn-warning',
						callback: fnNo
					}
				}
			});
		};

		$rootScope.alert = function(message, callback) {
			bootbox.alert(message, callback);
		};

	}])
	.filter('dateRange', function() {
		return function(list, from, to, attr) {
			if((from || to) && attr) {
				var filtered = [];
				var obj;
				if(from && to) {
					for(var i in list) {
						obj = list[i];
						complexField = getComplexValue(obj, attr);

						if(complexField >= from && complexField <= to) {
							filtered.push(obj);
						}
					}
				} else if(from) {
					for(var i in list) {
						obj = list[i];
						complexField = getComplexValue(obj, attr);

						if(complexField >= from) {
							filtered.push(obj);
						}
					}
				} else if(to) {
					for(var i in list) {
						obj = list[i];
						complexField = getComplexValue(obj, attr);

						if(complexField <= to) {
							filtered.push(obj);
						}
					}
				}
				return filtered;
			}
			return list;
		}
	})
	.constant('bootbox', window.bootbox);
