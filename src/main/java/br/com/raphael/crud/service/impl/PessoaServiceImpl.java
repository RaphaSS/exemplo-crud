package br.com.raphael.crud.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.raphael.crud.model.Pessoa;
import br.com.raphael.crud.repository.PessoaRepository;
import br.com.raphael.crud.service.PessoaService;

@Service
@Transactional
public class PessoaServiceImpl extends AbstractServiceImpl<Pessoa, Long> implements PessoaService {

	@Autowired
	public PessoaServiceImpl(final PessoaRepository repository) {
		super(repository);
	}

}
