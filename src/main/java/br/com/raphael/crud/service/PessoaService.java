package br.com.raphael.crud.service;

import br.com.raphael.crud.model.Pessoa;

public interface PessoaService extends AbstractService<Pessoa, Long> {}
