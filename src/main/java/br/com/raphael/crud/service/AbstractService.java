package br.com.raphael.crud.service;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AbstractService<T, ID extends Serializable> {

	T findOne(ID id);

	Page<T> getAllPaged(Pageable pageable);

	Iterable<T> getAll();

	T save(T entity);

	void delete(ID id);

}
