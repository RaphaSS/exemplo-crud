package br.com.raphael.crud.endpoint;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageNavigationController {

	@RequestMapping("/")
	public String index() {
		return "index";
	}

}
