package br.com.raphael.crud.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.raphael.crud.endpoint.AbstractRestController;
import br.com.raphael.crud.model.Pessoa;
import br.com.raphael.crud.service.PessoaService;

@RestController
@RequestMapping("/pessoa")
public class PessoaController extends AbstractRestController<Pessoa, Long> {

	@Autowired
	protected PessoaController(final PessoaService service) {
		super(service);
	}

}
