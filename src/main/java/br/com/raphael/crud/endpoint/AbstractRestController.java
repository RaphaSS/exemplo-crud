package br.com.raphael.crud.endpoint;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import br.com.raphael.crud.service.AbstractService;

@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractRestController<T, ID extends Serializable> {

	protected final Logger log = Logger.getLogger(getClass());

	protected final AbstractService<T, ID> service;

	protected AbstractRestController(final AbstractService<T, ID> service) {
		this.service = service;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public HttpEntity<?> getAll() {
		log.debug("GET /all");
		final List<T> lista = Lists.newArrayList(service.getAll());
		return new ResponseEntity(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public HttpEntity<?> get(@PathVariable("id") final ID id) {
		log.debug("GET /" + id);
		return new ResponseEntity(service.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public HttpEntity<?> add(@RequestBody final T entity) {
		log.debug("POST /add");
		try {
			service.save(entity);
			return new ResponseEntity(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public HttpEntity<?> update(@PathVariable("id") final ID id, @RequestBody final String json) throws JsonProcessingException,
		IOException {
		log.debug("PUT /" + id);
		try {
			final ObjectMapper objectMapper = new ObjectMapper();
			final T entity = objectMapper.readerForUpdating(service.findOne(id)).readValue(json);
			service.save(entity);
			return new ResponseEntity(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public HttpEntity<?> remove(@PathVariable("id") final ID id) {
		log.debug("DELETE /" + id);
		try {
			service.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
