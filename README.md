## Exemplo CRUD ##

Este projeto é um exemplo de aplicação com funcionalidades de CRUD com objetos representando pessoas descritas da seguinte forma:

```
{
  id: 1,
  nome: 'João da Silva',
  dataNascimento: 1465095600000, //millis
  cpf: '999.999.999-99'
}
```


### Como executar a aplicação? ###

O código fonte pode ser executado em servidores de aplicação (ex: [Apache Tomcat](http://tomcat.apache.org/)).
Os parâmetros de acesso ao banco de dados encontram-se no arquivo ```src/main/resources/application.properties```. A aplicação construirá o esquema de dados ao iniciar a execução.